<?php

/**
 * @todo make some options available in CMS behaviour tab
 */
class BBMediaSlideshow extends AbstractBBMediaSlideshow
{

	/**
	 * @config
	 * @var string
	 */
	private static $icon = 'tkibuildingblocks/images/icons/icon-mode-slideshow.png';

	public function prepare()
	{
		$htmlId = get_class($this) . '-'. $this->ID;

		Requirements::customScript(<<<JS
			$(document).ready(function(){
				$('#$htmlId').owlCarousel({
					autoWidth: false,
					autoHeight: true,
					loop: true,
					autoplay: true,
					autoplayTimeout: 4000,
					autoplayHoverPause: false,
					animateIn: 'fadeIn',
					animateOut: 'fadeOut',
					nav: true,
					navText: ['Prev','Next'],
					items: 1
				});
			});

JS
		);

	}

}
