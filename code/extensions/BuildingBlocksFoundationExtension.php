<?php

class BuildingBlocksFoundationExtension extends SiteTreeExtension
{

	/**
	 * Adds requirements
	 * @param object $controller
	 */
	function contentcontrollerInit($controller) {
		$require = Requirements::backend();
        if($require instanceof TkiRequirements_Backend) {
            $require->stylesheets(array(
                TKIBUILDINGBLOCKS_FOUNDATION_DIR .'/client/dist/styles/vendor.css',
                TKIBUILDINGBLOCKS_FOUNDATION_DIR .'/client/dist/styles/module.css'
            ),null,70);
            $require->js(TKIBUILDINGBLOCKS_FOUNDATION_DIR .'/client/dist/js/vendor.js','body',60);
            $require->js(TKIBUILDINGBLOCKS_FOUNDATION_DIR .'/client/dist/js/module.js','body',50);
        }
	}

}
