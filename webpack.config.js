const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const PATHS = {
	MODULES: './node_modules',
	SRC: './client/src',
	SRC_JS: './client/src/js',
	SRC_CSS: './client/src/styles',
	DIST: './client/dist',
	DIST_CSS: './client/dist/styles',
	DIST_JS: './client/dist/js'
};

const SUPPORTED_BROWSERS = [
  'Chrome >= 35',
  'Firefox >= 31',
  'Edge >= 12',
  'Explorer >= 9',
  'iOS >= 8',
  'Safari >= 8',
  'Android 2.3',
  'Android >= 4',
  'Opera >= 12',
];

module.exports = {
	entry: {
		'vendor': `${PATHS.SRC_JS}/vendor.js`,
		'module': `${PATHS.SRC_JS}/module.js`
	},
	resolve: {
		moduleDirectories: [PATHS.SRC_JS, PATHS.MODULES]
	},
	output: {
		path: PATHS.DIST,
		filename: 'js/[name].js'
	},
	externals: {
		jquery: 'jQuery',
		window: 'window'
	},
	devtool: 'source-map',
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: {
					presets: ['es2015'],
					plugins: ['transform-object-assign'],
					comments: false
				}
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract([
					'css-loader?sourceMap&minimize',
					'postcss-loader?sourceMap',
					'resolve-url-loader',
					'sass-loader?sourceMap'
				], {
					publicPath: '../'
				})
			},
			{
				test: /\.less$/,
				loader: ExtractTextPlugin.extract([
					'css-loader?sourceMap&minimize',
					'postcss-loader?sourceMap',
					//'resolve-url-loader',
					'less-loader?sourceMap'
				], {
					publicPath: '../'
				})
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract([
					'css-loader?sourceMap&minimize',
					'postcss-loader?sourceMap',
					'resolve-url-loader'
				], {
					publicPath: '../'
				})
			},
			{
				test: /\.(woff|woff2|otf|eot|ttf)$/,
				loader: 'file-loader?name=fonts/[name].[ext]'
			},
			{
				test: /\.(png|gif|jpg|svg)$/,
				loader: 'file-loader?name=images/[name].[ext]',
			}
		]
	},
	postcss: [
		autoprefixer({ browsers: SUPPORTED_BROWSERS })
	],
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			window: 'window'
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				unused: false,
				warnings: false
			}
		}),
		new ExtractTextPlugin('styles/[name].css')
	]

};
