/*
 * Vendor
 */

require('jquery');

//import Foundation from 'foundation-sites';
require('../styles/vendor.scss');

// Slideshow components - temporary, until Orbit is implemented
require('animate.css/animate.css');
require('owl.carousel/src/scss/owl.carousel.scss');
require('owl.carousel/src/scss/owl.theme.default.scss');

require('imports-loader?jQuery=jquery!owl.carousel');