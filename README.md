# Building Blocks Templates

This module provides templates for the Building Blocks module, with a dependency on Foundation Sites.

## Installation
The module may be installed with composer:

    composer require "tkisilverstripeteam/tkibuildingblocks-foundation:*"

