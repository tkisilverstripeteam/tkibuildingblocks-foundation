<div id="$ClassName-$ID" class="bb-block bb-html $FxTypeClasses">
	<% if $Title && $TitleTag && $TitleTag != 'hidden' %>
	<$TitleTag class="bb-layout__title $FxTitleClass">$Title
		<% if $SubTitle %><br /><span class="bb-layout__subtitle">$SubTitle</span><% end_if %>
	</$TitleTag>
	<% end_if %>
	<div class="bb-html__content">
		$Content
	</div>
</div>
