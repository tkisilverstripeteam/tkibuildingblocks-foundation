<div class="row bb-layout bb-layout-textmedia $ViewClass $FxTypeClasses">
	<div class="small-6 column bb-textmedia__text">
		<% if $Title && $TitleTag && $TitleTag != 'hidden' %>
		<$TitleTag class="bb-layout__title bb-textmedia__title $FxTitleClass">$Title</$TitleTag>
		<% end_if %>
		<% if $Content %>
		<div class="bb-textmedia__content">$Content</div>
		<% end_if %>
	</div>
	<% if $MediaView.items %>
	<div class="small-6 column bb-textmedia__media">
		$MediaView
	</div>
	<% end_if %>
</div>
